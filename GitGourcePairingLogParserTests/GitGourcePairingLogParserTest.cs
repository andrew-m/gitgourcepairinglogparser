﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GitGourcePairingLogParser;
using NUnit.Framework;

namespace GitGourcePairingLogParserTests
{
    [TestFixture]
    public class GitGourcePairingLogParserTest
    {
        [Test]
        public void ShouldReturnAUserStringFromAUserString()
        {
            LogParser logParser = new LogParser();

            String result = logParser.Parse("user: something");

            Assert.That(result.StartsWith("user: "), "Result should start with \"user: \"");
        }
    }
}
